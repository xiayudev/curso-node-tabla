const { crearArchivo } = require('./helpers/multiplicar')
const argv = require('./config/yargs')

/* Print mult x 5 */
console.clear(); // Clean the console before executing

//console.log( argv );

crearArchivo( argv.base, argv.listar, argv.hasta )
    .then( nameFile => console.log(`El archivo ${nameFile} fue creado!`) )
    .catch( err => console.log );
