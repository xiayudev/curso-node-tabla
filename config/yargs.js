const argv = require('yargs')
    .options({
        'b': {
            alias: 'base',
            type: 'number',
            demandOption: true,
            describe: 'Es la base de la tabla de multiplicar'
        },
        'l': {
            alias: 'listar',
            type: 'boolean',
            default: false,
            describe: 'Muestra la tabla en consola'
        },
        'h': {
            alias: 'hasta',
            type: 'number',
            default: 10,
            describe: 'Hasta dónde quieres multiplicar'
        }
    })
    .check((argv, option) => {
        if ( isNaN( argv.base ) ) throw new Error('La base debe ser un número');
        return true;
    })
    .argv;

module.exports = argv;