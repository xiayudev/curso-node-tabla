const fs = require('fs');

const crearArchivo = async ( base = 1, listar, hasta ) => {

    try {
        let salida = '';
        for (let i = 0; i <= hasta; i++) {
            salida += `${base} x ${i} = ${base * i}\n`;
        }

        if ( listar ) {
            console.log('====================');
            console.log(`   Tabla del ${base}:`);
            console.log('====================');
            console.log(salida);
        }
    
        /**Create a file */
        fs.writeFileSync(`./out/tabla-${base}.txt`, salida);
        return `tabla-${base}.txt`;    
    } catch (error) {
        throw error;
    }
    
};

module.exports = {
    crearArchivo
};